import fs from 'fs-extra';
import * as sqlite3 from 'sqlite3';
import Category from '../../shared/interfaces/Category';
import { CategoryRow } from '../../shared/interfaces/Category';
import Song from '../../shared/interfaces/Song';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const DatabaseManager = (() => {
    const _resourcePath = 'resources';
    const _databasePath = `${_resourcePath}/database.db`;
    const _db = new sqlite3.Database(_databasePath);

    fs.ensureDirSync(_resourcePath);
    initDatabase();

    function initDatabase(): void {
        try {
            console.log('Successfully connected to databases.');
            const createCategoryTableQuery = `CREATE TABLE IF NOT EXISTS categories (
                id TEXT PRIMARY KEY,
                name TEXT,
                pointTotal INTEGER
            )`;
            const createSongTableQuery = `CREATE TABLE IF NOT EXISTS songs (
                id TEXT PRIMARY KEY,
                songName TEXT,
                gameName TEXT,
                pointValue INTEGER,
                audio BLOB,
                audioMIME TEXT,
                image BLOB,
                imageMIME TEXT,
                categoryID INTEGER,
                FOREIGN KEY(categoryID) REFERENCES categories(id)
            )`;
            _db.run(createCategoryTableQuery).run(createSongTableQuery);
        } catch (err: unknown) {
            if (err instanceof Error) console.error(err.message);
            else console.log(`Unknown error: ${err}`);
        }
    }

    async function getCategories(): Promise<Category[]> {
        try {
            const categories: Category[] = [];
            const _db = new sqlite3.Database(_databasePath);
            const categoryRows: CategoryRow[] = await new Promise((resolve, reject) => {
                _db.all('SELECT * FROM categories', (err, rows: CategoryRow[]) => {
                    if (err) reject(err);
                    else resolve(rows);
                });
            });
            for (const category of categoryRows) {
                const songs: Song[] = await getSongs(category.id);
                categories.push({ ...category, songs });
            }
            return categories;
        } catch (error: unknown) {
            console.error('Error retreiving categories: ', error);
            return [];
        }
    }

    async function addCategory(category: Category): Promise<void> {
        _db.run('INSERT INTO categories (id, name, pointTotal) VALUES (?,?,?)', category.id, category.name, category.pointTotal);
        category.songs.forEach(async (song) => {
            await addSong(song);
        });
    }

    async function deleteCategory(categoryID: string): Promise<void> {
        _db.run('DELETE FROM `categories` WHERE `id` =?', categoryID);
    }

    async function addSong(song: Song): Promise<void> {
        _db.run(
            'INSERT OR IGNORE INTO `songs` (`id`, `songName`, `gameName`, `pointValue`, `audio`, `audioMIME`, `image`, `imageMIME`, `categoryID`) VALUES (?,?,?,?,?,?,?,?,?)',
            song.id,
            song.songName,
            song.gameName,
            song.pointValue,
            Buffer.from(song.audio),
            song.audioMIME,
            Buffer.from(song.image),
            song.imageMIME,
            song.categoryID,
        );
    }

    async function getSong(categoryID: string, songID: string): Promise<Song> {
        const song: Song = await new Promise((resolve, reject) => {
            _db.run('SELECT * FROM songs WHERE categoryID = ? AND songID = ?', categoryID, songID, (err, song: Song) => {
                if (err) reject(err);
                else resolve(song);
            });
        });
        return song;
    }

    async function getSongs(categoryID): Promise<Song[]> {
        const songs: Song[] = await new Promise((resolve, reject) => {
            _db.all('SELECT * FROM songs WHERE categoryID = ?', categoryID, (err, rows: Song[]) => {
                if (err) reject(err);
                else resolve(rows);
            });
        });
        return songs;
    }

    async function deleteSong(songID: string): Promise<void> {
        const _db = new sqlite3.Database(_databasePath);
        _db.run('DELETE FROM `songs` WHERE `id` =?', songID);
    }

    async function categoryExists(categoryID: string): Promise<boolean> {
        try {
            const exists: boolean = await new Promise((resolve, reject) => {
                _db.get(`SELECT EXISTS (SELECT 1 FROM categories WHERE id=?) as 'exist'`, [categoryID], (err, row: CategoryRow) => {
                    if (err) reject(err);
                    else resolve(row['exist'] === 1);
                });
            });
            return exists;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    function closeDatabase(): void {
        _db.close();
    }

    return { getCategories, addCategory, deleteCategory, addSong, getSong, getSongs, deleteSong, categoryExists, closeDatabase };
})();

export default DatabaseManager;
