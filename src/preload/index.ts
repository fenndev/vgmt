import { contextBridge, ipcRenderer } from 'electron';
import { electronAPI } from '@electron-toolkit/preload';
import type Category from '../shared/interfaces/Category';
import Song from '../shared/interfaces/Song';

// Custom APIs for renderer
const api = {
    sendFile: (file: Song): void => {
        console.log('Running!');
        ipcRenderer.send('add-song', file);
    },
    deleteSong: (songID: string): void => {
        ipcRenderer.send('delete-song', songID);
    },
    addCategory: (category: Category): void => {
        console.log('Adding category: ', category);
        ipcRenderer.send('add-category', category);
    },
    fetchCategories: async (): Promise<Category[]> => {
        return await ipcRenderer.invoke('fetch-categories');
    },
    categoryExists: async (categoryID: string): Promise<boolean> => {
        console.log('Checking for Category...');
        return await ipcRenderer.invoke('category-exists', categoryID);
    },
};

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
    try {
        contextBridge.exposeInMainWorld('electron', electronAPI);
        contextBridge.exposeInMainWorld('api', api);
    } catch (error) {
        console.error(error);
    }
} else {
    // @ts-ignore (define in dts)
    window.electron = electronAPI;
    // @ts-ignore (define in dts)
    window.api = api;
}
