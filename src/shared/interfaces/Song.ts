export default interface Song {
    id: string;
    songName: string;
    gameName: string;
    pointValue: number;
    audio: ArrayBuffer | Buffer;
    audioMIME: string;
    image: ArrayBuffer | Buffer;
    imageMIME: string;
    categoryID: string;
}
