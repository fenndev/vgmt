import type Song from './Song';

export interface CategoryRow {
    id: string;
    name: string;
    pointTotal: number;
}

export default interface Category extends CategoryRow {
    songs: Song[];
}
